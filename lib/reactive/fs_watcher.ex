defmodule Reactive.FsWatcher do
  use Reactive.TemporaryEntity
  require Logger

  def start(directory) do
    Reactive.Entity.request([Reactive.FsWatcher, directory], :start)
  end

  def init(args = [directory]) do
    fs_watcher = :erlang.binary_to_atom("fs_watcher_" <> directory, :utf8)
    state = %{
      fs_watcher: fs_watcher,
      started: :false
    }
    :fs.start_link(state.fs_watcher, directory)
    :fs.subscribe(state.fs_watcher)
    {:ok, state, %{}}
  end

  def info({_watcher_process, {:fs, :file_event}, {changedFile, type}}, state) do
    Logger.debug("FS EVENT #{inspect changedFile} #{inspect type}")
    changed_file = to_string(changedFile)
    case File.stat(changed_file) do
      {:error, _reason} -> # removed
        Reactive.Entity.event([Reactive.File, changed_file],{:change, type})
        Reactive.Entity.event([Reactive.Directory, changed_file],{:change, type})
        Reactive.Entity.event([Reactive.Directory, Path.dirname(changed_file)],{:change, type})
      {:ok, info} ->
        case info.type do
          :directory -> Reactive.Entity.event([Reactive.Directory, changed_file],{:change, type})
          :regular -> Reactive.Entity.event([Reactive.File, changed_file],{:change, type})
        end
    end
    if(Enum.member?(type,:renamed) || Enum.member?(type,:created) || Enum.member?(type,:removed)) do
      Reactive.Entity.event([Reactive.Directory, Path.dirname(changed_file)],{:change, [:member]})
    end
    state
  end

  def request(:start, state, _from, _rid) do
    {:reply, :ok, state}
  end

  def can_freeze(_state, _observed) do
    false
  end
end