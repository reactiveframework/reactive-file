defmodule Reactive.File do
  use Reactive.Entity

  def init(args = [filename]) do
    {mod_ts, removed} = case File.stat(filename) do
      {:error, _reason} ->
        {Reactive.Entity.timestamp(),:true}
      {:ok, info} ->
        {(:calendar.datetime_to_gregorian_seconds(info.mtime) - 62167219200)*1000,:false}
    end

    {:ok,%{
      filename: filename,
      modification_ts: mod_ts,
      content_observation: false,
      ts_content_observation: false,
      removed: removed
    }, %{}}
  end

  def observers(:ts_content,state,[],_) do
    %{state | ts_content_observation: true}
  end
  def observers(:ts_content,state,_,[]) do
    %{state | ts_content_observation: false}
  end
  def observers(:content,state,[],_) do
    %{state | content_observation: true}
  end
  def observers(:content,state,_,[]) do
    %{state | content_observation: false}
  end
  def observers(_,state,_,_) do
    state
  end

  def file_modified(state,ts) do
    if(state.ts_content_observation || state.content_observation) do
      content = case File.read(state.filename) do
        {:ok, data} -> data
        {:error, _} -> :not_found
      end
      notify_observers(:ts_content, {:set, [{ts, content}]})
      notify_observers(:content, {:set, [content]})
    end
    notify_observers(:ts, {:set, [ts]})
  end

  def retrive(id) do
    case Reactive.Entities.retrive_entity(id) do
      :not_found -> :not_found
      {:ok,%{ state: state, container: container}} ->
        mod_ts = case File.stat(state.filename) do
          {:error, _reason} ->
            Reactive.Entity.timestamp()
          {:ok, info} ->
            (:calendar.datetime_to_gregorian_seconds(info.mtime) - 62167219200)*1000
        end
        tco = case Map.get(container.observers,:ts_content,[]) do
          [] -> false
          _ -> true
        end
        co = case Map.get(container.observers,:content,[]) do
          [] -> false
          _ -> true
        end
        state = %{ state | ts_content_observation: tco, content_observation: co }
        if(mod_ts != state.modification_ts)  do
          file_modified(state,mod_ts)
        end
        {:ok,%{
          state: %{ state | modification_ts: mod_ts },
          container: container
        }}
    end

  end

  def event({:change,_type},state,_from) do
    ts = Reactive.Entity.timestamp()
    file_modified(state,ts)
    %{ state | modification_ts: ts }
  end

  def get(:content,state) do
    content = case File.read(state.filename) do
      {:ok, data} -> data
      {:error, _} -> :not_found
    end
    {:reply, content, state}
  end

  def get(:ts_content,state) do
    content = case File.read(state.filename) do
      {:ok, data} -> data
      {:error, _} -> :not_found
    end
    {:reply, {state.modification_ts,content}, state}
  end

  def get(:ts,state) do
    {:reply, state.modification_ts, state}
  end

  def request({:write, content},state,_from,_rid) do
    File.write!(state.filename,content)
    {:reply, :ok, state}
  end

end