defmodule Reactive.Directory do
  use Reactive.Entity
  require Logger

  def init(_args = [filename]) do
    mod_ts = case File.stat(filename) do
      {:error, _reason} ->
        Reactive.Entity.timestamp()
      {:ok, info} ->
        (:calendar.datetime_to_gregorian_seconds(info.mtime) - 62167219200)*1000
    end

    {:ok,%{
      filename: filename,
      modification_ts: mod_ts,
      content_observation: false,
      ts_content_observation: false
    }, %{}}
  end

  def retrive(id) do
    case Reactive.Entities.retrive_entity(id) do
      :not_found -> :not_found
      {:ok,%{ state: state, container: container}} ->
         mod_ts = case File.stat(state.filename) do
           {:error, _reason} ->
             Reactive.Entity.timestamp()
           {:ok, info} ->
             (:calendar.datetime_to_gregorian_seconds(info.mtime) - 62167219200)*1000
         end
         tco = case Map.get(container.observers,:ts_content,[]) do
           [] -> false
           _ -> true
         end
         co = case Map.get(container.observers,:content,[]) do
           [] -> false
           _ -> true
         end
         state = %{ state | ts_content_observation: tco, content_observation: co }
         if(mod_ts != state.modification_ts)  do
           dir_modified(state,mod_ts)
         end
         {:ok, %{
           state: %{ state | modification_ts: mod_ts },
           container: container
         }}
    end
  end

  def observers(:ts_content,state,[],_) do
    %{state | ts_content_observation: true}
  end
  def observers(:ts_content,state,_,[]) do
    %{state | ts_content_observation: false}
  end
  def observers(:content,state,[],_) do
    %{state | content_observation: true}
  end
  def observers(:content,state,_,[]) do
    %{state | content_observation: false}
  end
  def observers(_,state,_,_) do
    state
  end


  def dir_modified(state, ts) do
    Logger.debug("DIR MODIFIED! #{state.filename} : #{inspect state}")
    if(state.ts_content_observation || state.content_observation) do
      content = case File.ls(state.filename) do
        {:ok, data} -> data
        {:error, _} -> :not_found
      end
      notify_observers(:content, {:set, [content]})
      notify_observers(:ts_content, {:set, [{ts, content}]})
    end
    notify_observers(:ts, {:set, [ts]})
  end

  def event({:change, _type}, state, _from) do
    ts = Reactive.Entity.timestamp()
    dir_modified(state,ts)
    %{ state | modification_ts: ts }
  end

  def get(:content, state) do
    content = case File.ls(state.filename) do
      {:ok,files} -> files
      {:error,_} -> :not_found
    end
    {:reply, content, state}
  end

  def get(:ts_content, state) do
    content = File.ls!(state.filename)
    {:reply, {state.modification_ts, content}, state}
  end

  def get(:ts,state) do
    {:reply, state.modification_ts, state}
  end
end